# Markdown

Arbeidskravene i _Metadata interoperabilitet_ skal leveres i Markdown. Dette notebook viser eksempel på markdown og gir mer informasjon. Det er nyttig å lære markdown fordi det kan brukes for å dokumentere koden i en Jupyter Notebook. Markdown brukes ofte i andre sammenhenger i dataverden, f.eks. Github / Gitlab.

Det er også nyttig å forstå markeringsspråk. I undervisningen har vi sett på XML og HTML. Markdown er en mye enkelere markeringsspråk.

Markdown kan skrives i:

* Teksteditor
* Jupyter

## Skriv Markdown i en teksteditor

Markdown kan skrives i Jupyter eller en hvilken som helst teksteditor (VS Code, Atom, Notebook++, TextPad, Sublime Text, ...). Hvis du bruke en teksteditor skal filen ha .md som etternavn, f.eks: _david-massey-arbeidskrav-1.md_. Filen sendes som vedlegg til David.


## Skriv Markdown i Jupyter

En Jupyter notebook består av celler. Disse kan være kode eller markdown. En notebook får automatisk .ipynb som etternavn, f.eks. _david-massey-arbeidskrav-1.ipynb_. Send filen som vedlegg til David.

Dette celle er skrevet i markdown. VIKTIG: Dobbeltklikk på cellen for å se / redigere markeringene.

For å skrive Markdown i Jupyter endre du cellen fra kode (__Code__) som er utgangspunktet til __Markdown__. Dette gjøres i listen som vises i knapperaden.

Når du kjører en Markdown celle formateres koden (NB dette skjer ikke i en teksteditor, da ser du kun kodene).

Her har jeg valgt å legge alt i en celle. Alternativt kunne teksten vært delt opp i flere Markdown-celler, med samme resultat.


## Kodene

### Overskriftene

Overskrifter markeres med #-tegn. Antall # angir nivået til overskriften, dvs. # er den hovedoverskrift (tilsvarer h1 i HTML). ## er en under-overskrift (tilsvarer h2 i HTML). Det er sjelden at du trenger mer enn ### (h3). Markdown har seks nivåer som i HTML.

### Avsnitt

Det er ingen kode for avsnitt. Teksten som ikke er markert er en avsnitt. For å starte et nytt avsnitt legg inn en tom linje. Enkelt!

### Kursiv og uthevet

Kursiv skrives med å sette \_ foran og etter teksten som skal settes i _kursiv_. \* kan også brukes for *kursiv*. For å utheve brukes to \_ eller \*, slik **uhevet tekst** eller __uthevet tekst__.

### Lister

Det skilles mellom sortert (ol i HTML) og utsortert liste (ul i HTML).

I en sortert liste brukes tall etterfulgt av punktum og et mellomrom (1. listepunkt). F.eks:

Produksjonslinje (sortert):

1. Innhøsting av data
2. Analyse av datakvalitet og datavasking
3. Berikelse
4. Transformasjoner
5. Publisering som RDF

I en usortert liste brukes * etterfulgt av mellomrom. F.eks.

Grunnformater (usortert):

* CSV
* XML
* JSON

## Mer informasjon

Over har jeg beskrevet de mest grunnleggende koder, det finnes noen flere, f.eks. for å skrive tabeller. Se ressursene under uke 1 på fagsiden (https://oslometabi.gitlab.io/mbib4140/h22/fagside/mbib4140.html) for flere koder og eksempler. Det finnes også mange ressurser på nettet.

Som alltid. Det er lov å spøre (david.massey@oslomet.no)!