# Sensurveileder MBIB4140-H21

## Om eksamen

Eksamen er en semesteroppgave som har en omfang på 15 sider (35.000 tegn inkludert mellomrom), eller den kan skrives som gruppeoppgave. Omfang skal da være på ca. 20 sider (46.000 tegn inkludert mellomrom). Det er tre type oppgave som kan leveres:

1. Produksjonslinje
2. Klassisk oppgave
3. Kombinasjon

## Produksjonslinje

### Innleveringformat

Kandidatene kan levere den skriftlige delen av eksamen enten som PDF eller i markdown (.md) format. Kandidatene kan da laste opp et enkeltdokument hvis de ønsker kun å levere en skriftlig redegjørelse. Dersom kandidatene ønsker å inkludere en eller flere notebook kan de eksporteres til html format og inkludere denne. Dersom kandidatene ønsker å referere til python kode med linjenummer, kan pythonkoden inkluderes i en tekst fil med filendelse .py.

Merk at hvis besvarelsen består av mer enn en fil må alle filene pakkes inn i en ZIP-fil (filendelse .zip). Det er ZIP-filen som lastes opp.

Eksamen har hatt fokus på en "produksjonslinje" som omhandler følgende tematikker:

* Datainnsamling
* Beskrivelse
* Datakvalitet
* Transformasjon
* Berikelse
* Søkbarhet / gjenbruk

Kandidatene vurderes ikke på sitt evne til å skripte pythonkode. Oppgaven vurderes ut ifra de refleksjonene kandidatene gjør rundt produksjonslinjen sin. Kandidatene selv velger hva de ønsker å fokusere på i oppgaven, og det er ikke noe krav at hver del skal være like stor. Det kan være at en oppgave fokuserer mye på datakvalitetproblematikken fordi det var utfordrende. Dette kan da gå på bekostning av de andre delene. Men oppgaven skal være innom alle tematikkene nevnt over. Kandidatene kan skrive at de har vurdert en tematikk feks datakvalitet, men at det ikke oppleves som relevant for oppgaven. Berikelse og transformasjon kan slås sammen til en underdel, dersom det oppleves som fornuftig.

### Datainnsamling

Kandidatene kan hente og jobbe med data fra datasett de selv har funnet eller fra et eller flere API. Selv om det er ønskelig at det jobbes med minst to datasett som kan kobles sammen, så er minimumskravet et datasett. Datasettet kan for såvidt være selvlaget. Det vil naturligvis være vanskeligere å jobbe med datasett som innhøstes fra et API, sammenlignet med ferdige datasett. Datasett fra API scorer ikke nødvendigvis høyere enn ferdige datasett, men kandidatene kan belønnes hvis kandidatene dokumenter på en overbevisende måte at datainnsamlingen har vært krevende.

### Beskrivelse
Kandidatene skal beskrive konteksten til materialet. Beskrivelsen skal tydelig identifisere kilden til materialet, hvem som har samlet det inn, hvorfor, og eventuelt hvordan det er blitt forvaltet. Innhold og omfang skal også beskrives. Dette kan inkludere tidsperspektivet for materialet og hva dataene omhandler. Hvis dataene kommer fra et API kan det være vanskeligere å beskrive omfang og innhold, men kandidaten skal ha gjort et forsøk.

### Datakvalitet

Det skal være en refleksjon rundt kvaliteten til dataene som passer i stil til datautvalget. Hvis kvaliteten oppleves som høy skal det redegjøres for hvorfor. Det samme om det oppleves som lavt, da kan kandidatene si litt om hvorfor dårlig kvalitet er et problem i deres produksjonlinje. Det skal dokumenteres hvilken forbedringer av datakvalitet som er gjort.

### Transformasjon

En transformasjon kan være å konvertere data fra et format til en annet. En transformasjon kan også være å fikse problemer med dataene i feks openrefine eller annet for å gjøre dataene brukbar. Noen kandidater vil kanskje trenge å gjøre en transformasjon fra JSON til CSV eler lignende. Dette skal dokumenteres. Andre temaer som kan diskuteres er utvelgelse av kolonner og/eller rader, vasking av data, mapping til andre skjemaer. Kandidatene vil trenge å gjøre en transformasjon til å få dataene over til RDF.

### Berikelse

En berikelse av datasettet kan være å koble data i datasettet til andre kilder, eller på en eller annen måte dele opp eller koble data for å berike datasettet. Berikelse kan også være å koble dataene til annet data feks når det er i RDF. For noen kan det være å lenke til et annet datasett. Utgangspunktet er at de utvider et datasett fra en annen datasett. Relevante temaer her er utfordring med matching og rollen identifikatorer spiller. Merk at berikelse vil sannsynligvis være veldig avhengig av datakildene kandidatene jobber med.

Begrepene berikelse og transformasjon kan i noen sammenheng overlappe. En berikelse kan være en transformasjon, men en transformasjon trenger ikke være en berikelsen.

### Søkbarhet / gjenbruk

Sluttproduktet av produksjonslinjen skal helst være en semantisk web beskrivelse av data som det kan være mulig å søke i med sparql. Kandidatene skal helst gjenbruke eksisterende ontologier framfor å finne opp nye. Hvis kandidatene har en data-domene som har dårlig tilsvarende ontologier, kan kandidatene prøve å lage sine egne. Men den evnen ligger litt utenfor forventet kunnskaper til kandidatene. Det forventes ikke mye arbeid på ontologi nivå, men det skal komme fram hvordan kandidatene klarer å konvertere dataene sine til RDF, mapping til relevante predikater (properties) / klasser, hvilken ontologier er brukt og søkeeksempler med sparql. Kandidatene bør også redegjøre for hvordan de ser for seg andre kan bruke sin RDF-data. Merk. RDF kom sent i semesteret og flere studenter reagerte på det de merer er ekstra. Linked-data er også et stort område. Emnet valgte bredde framfor dybde så det forventes ikke ikke mye dybde her. 

### Andre kommentarer

Det er ikke ønskelig at oppgaven skal inneholde mye python kode. Stedvis i oppgaven kan kandidatene eksemplifisere pythonkode for å vise at de har løst noe på en interessant måte, feks hvordan de fikset problemer med datakvaliteten eller hvordan skriptet koblet data sammen

Python kode kan legges ved som vedlegg. Koden skal da være i en eller flere .py filer. Kandidatene kan referere til filnavn:linjenummer i pythonfiler fra besvarelsen dersom det er noe spesielt de ønsker at sensor skal se på. Husk kandidatene vurderes kun på refleksjonen rundt produksjonslinjen.

### Karakterfastsetting

Forelesninger og gjennomgang av pensum i emnet gir kandidatene en ferdigdefinert utgangspunkt for 'relevant faglig teori og metoder'. En god prestasjon vil gjenbruke denne. Hvis kandidatene benytter dette på en måte som gir mye innsikt eller velger å inkludere andre måter å gjennomføre produksjonslinje vil det telle høyere. Hvis kandidatene velger å ikke benytte det som er gjennomgått og har ikke en annen måte å forklare produksjonslinjen vil det telle svakere.
Merk det er ikke nødvendigvis mengde som utgjør forskjellen mellom oppgavene. Det er både gruppe og individuell innleveringer. Kandidatene vurderes ut ifra refleksjoner rundt produksjonslinjen.

Det er ikke noe enkel og tydelig definisjon om forskjellen mellom "god", "meget god" og "fremragende". Hver oppgave vurderes individuelt og det er helheten leseren sitter igjen med som avgjør hvordan oppgaven oppleves. Noen stikkord som kan være relevant her er *arbeidsmengde*, *resonnement*, *språklig engasjement* og *evne til refleksjon*. Arbeidsmengde skal ta høyde for gruppestørrelse, men det er formidlingen som avgjør. En oppgave kan ha en mindre datasett, men datasettet kan ha vært krevende å jobbe med. En annen oppgave kan ha mye data fra flere kilder, men som kan ha vært enkelt å jobbe med. Kandidatene må redegjøre for dette, de må dokumentere arbeidsmengde på en eller annen måte. Resonnementet bak produksjonslinjen er viktig. Det er tilfredsstillende å ha en resonnement som baserer seg på at datasettene i utgangpunktet bare er valgt for å løse et problem, men når kandidater jobber med data de brenner for vil det oppleves mer rikt. Refleksjon handler om at kandidatene klarer å ha et fugleperspektiv på det vi har jobbet med i løpet av semesteret. De evner å vise at de mestrer emnet på en mer teoretisk måte, altså ikke bare skrive om det tekniske de har jobbet med.

#### F : Ikke bestått

Kandidatene viser ikke tilstrekkelig innsikt i metadata og interoperabilitet. Kandidatene evner ikke å utarbeide en ordentlig produksjonslinje. Produksjonslinjen leverer ikke nødvendigvis et sluttprodukt, og hvis den er beskrevet er det usannsynlig at den kan gjenbrukes. Arbeidet viser at kandidaten ikke har tilstrekkelig innsikt og fagkunnskaper. Arbeidet fremstår som tilstrekkelig men tydelig mangler et innslag av kreativitet. Analyse og diskusjon har vesentlige mangler. Kandidaten viser liten evne til refleksjon. Fremstillingen har vesentlige mangler mht. form, formidling, struktur og språk.

#### E : Tilstrekkelig

Kandidatene viser tilstrekkelig innsikt i metadata og interoperabilitet. Kandidatene kan utarbeide en produksjonslinje, men det mangler tilstrekkelig resonnement. Produksjonslinjen leverer ikke nødvendigvis et sluttprodukt, og hvis den er beskrevet er det usannsynlig at den kan gjenbrukes. Arbeidet viser at kandidaten har tilstrekkelig innsikt og fagkunnskaper. Arbeidet fremstår som tilstrekkelig, men det er tydelig at det mangler et innslag av kreativitet. Analyse og diskusjon er tilstrekkelig fundert og koblet til produksjonslinjen. Kandidaten viser tilstrekkelig evne til refleksjon. Fremstillingen er stort sett akseptabel, men har merkbare mangler mht. form, formidling, struktur og språk.

#### D : Nokså god

Kandidatene viser nokså god innsikt i metadata og interoperabilitet. Kandidatene kan utarbeide en produksjonslinje, men det mangler resonnement. Produksjonslinjen leverer et sluttprodukt, men det er ikke gitt at denne kan gjenbrukes. Arbeidet viser at kandidaten har nokså god innsikt og viser nokså gode fagkunnskaper. Arbeidet fremstår som nokså godt men mangler et innslag av kreativitet. Analyse og diskusjon er nokså godt faglig fundert og koblet til produksjonslinjen. Kandidaten viser nokså god evne til refleksjon. Form, formidling, struktur og språk ligger på et akseptabelt nivå.

#### C : God prestasjon

Kandidatene viser god innsikt i metadata og interoperabilitet. Kandidatene kan utarbeide en relevant og tydelig produksjonslinje. Produksjonslinjen leverer et sluttprodukt som gjør at dataene kan gjenbrukes feks RDF og sparql. Arbeidet viser at kandidaten har god innsikt og viser gode fagkunnskaper. Arbeidet fremstår som godt med et innslag av kreativitet. Analyse og diskusjon er faglig godt fundert og tydelig koblet til produksjonslinjen. Kandidaten viser god evne til refleksjon. Form, formidling, struktur og språk ligger på et godt nivå.

#### B : Meget god prestasjon

Kandidatene viser meget god innsikt i metadata og interoperabilitet. Kandidatene kan utarbeide en relevant og tydelig produksjonslinje som er meget overbevisende. Produksjonslinjen leverer et sluttprodukt som gjør at dataene kan gjenbrukes feks RDF og sparql. Arbeidet viser at kandidaten har meget god innsikt og viser meget gode fagkunnskaper. Arbeidet fremstår som meget godt med et innslag av kreativitet og/eller oppleves som nyskapende. Analyse og diskusjon er meget godt faglig fundert og tydelig koblet til produksjonslinjen. Kandidaten viser meget god evne til refleksjon. Form, formidling, struktur og språk ligger på et meget høyt nivå.

#### A : Fremragende prestasjon

Kandidatene viser svært høy innsikt i metadata og interoperabilitet. Kandidatene kan utarbeide en relevant og tydelig produksjonslinje som er meget overbevisende. Produksjonslinjen leverer et sluttprodukt som gjør at dataene kan gjenbrukes feks RDF og sparql. Arbeidet viser at kandidaten har svært høy innsikt og viser svært gode fagkunnskaper. Arbeidet fremstår som fremragende med et høyt innslag av kreativitet og/eller oppleves som nyskapende. Analyse og diskusjon er svært godt fundert i det faglige og tydelig koblet til produksjonslinjen. Kandidaten viser svært god evne til refleksjon. Form, formidling, struktur og språk ligger på et svært høyt nivå.

## Valgfri tematikk

Det er ikke gitt at alle studenter vil klare å tilegne seg skripting ferdigheter i python. Derfor er det mulig å levere en tradisjonell semesteroppgave. Kandidatene vil ha måttet jobbe med en produksjonslinje på en eller annet nivå i løpet av semesteret i emnet. Oppgaven skal da vurderes utifra følgende kriteria:

### Problemstilling

Hvor interessant, original og velbegrunnet er problemstillingen?

### Metode og materiale

Hvor godt egner metode og materiale seg for å belyse problemstillingen? Det er en semesteroppgave så det er begrenset hvor mye metode som forventes, men det bør være mulig å se at kandidaten følger en metode. Kandidaten skal helst være tydelig på metoden.

### Klarhet

Hvor klart er arbeided presentert?

### Eksempelbruk

Hvor omfattende, klargjørende og relevant er eksemplifiseringen?

### Fokusering

Hvor godt klarer kandidaten å legge ut en rød tråd til problemstillingen gjennom hele oppgaven og unngå frie og uvesentlige assosiasjoner underveis. Holder kandidaten seg til hovedsporet?

### Dokumentasjon

Hvor godt blir konklusjonene underbygd?

### Dimensjonering

Er oppgaven passe lang? Hvor godt er de enkelte elementene i oppgaven dimensjonert i forhold til hverandre?

### Språkbruk

Hvor feilfritt, klart, interessevekkende og engasjerende er oppgaven skrevet?

### Karakterfastsetting

#### A

Kandidaten svarer på problemstillingen. Uvanlig klare, selvstendige og interessante resonnementer og relevante eksempler. Klart og presist språk, klare sammenhenger, meget godt fokusert og gjennomgående god struktur. Overbevisende dokumentasjon. Original tilnærming. Godt dimensjonert; formen er tilpasset innholdet. En svært god prestasjon.

#### B

Kandidaten svarer på problemstillingen. Klare, selvstendige resonnementer og relevante eksempler. Meget god fokusering. Klart og presist språk; gjennomgående god struktur. Meget god dokumentasjon. Godt dimensjonert; formen er tilpasset innholdet. En meget god prestasjon.

#### C

Kandidaten svarer på problemstillingen, og det meste er relevant. God forståelse og god dokumentasjon, men en noe mangelfull indre konsistens. For eksempel: originalt anslag, men skriver seg litt bort. Litt uklart språk eller rot i strukturen. Stort sett greitt dimensjonert. En gjennomsnittlig, god prestasjon, men ikke meget god.

#### D

Kandidaten prøver tydelig å besvare problemstillingen. Noe er relevant, men mye er også irrelevant, unødvendig, feilaktig eller mangelfullt. Noe rot i struktur og språk. Ikke helt godt dimensjonert. En prestasjon under middels.

#### E

Kandidaten har en problemstilling, og prøver å belyse den, men svært lite av oppgaven er relevant og godt. Store og vesentlige mangler i dokumentasjon og resonnement. Tilfeldige eksempler framfor drøfting/empiri. Oppgaven kan i en viss forstand være en løsning som under kategori D, men den er tynnere, mer rotete, dårligere disponert og mer naiv. Dårlig språk. En temmelig dårlig prestasjon.

#### F

Kandidaten mangler problemstilling eller besvarer den ikke. Dokumentasjonen er dårlig; språket er upresist/naivt/slurvete/ubehjelpelig, resonnementene er uklare og strukturen er uklar. En meget svak prestasjon.

## Kombinasjon

Det er mulig å skrive en tradisjonell semesteroppgave som som tar utgangspunkt i produksjonslinjen, men der kandidaten fokuserer på en eller flere deler av produksjonslinjen. Det kan være å ha fokus på datainnsamling og datakvalitet eller datasett forvaltning. Oppgaven vil vurderes da på samme måte som "Valgfri tematikk" der det forventes en tydelig problemstilling og konklusjon.

