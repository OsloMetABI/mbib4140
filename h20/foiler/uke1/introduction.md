name: inverse
layout: true
class: center, middle, inverse
---
# MBIB4140
## Metadata og interoperabilitet
### Høst 2020
---
layout: false
# Lærere
&nbsp;
.left-column-fifty[
    ![David Massey](../../fagside/assets/img/davm.jpg)<p>davm@oslomet.no</p>
]
.right-column-fifty[
    ![Thomas Sødring](../../fagside/assets/img/tsodring.jpg)<p>tsodring@oslomet.no</p>
]
---
class: center, middle
## ... _Vi lever i en datadrevet verden_ ...
---
.left-column[
  ## Data
]
.right-column[
* Data er den nye oljen
* Vi skal foredle denne nye oljen
  * Orientere oss i landskapet og fange data
  * Transformere data
  * Kvalitetsikre data
  * Berike data
* Vi skal jobbe med 'ekte' data
]
---
class: center
## Tre dimensjoner
![Data, Verktøy og prosesser](../../fagside/assets/img/uke1/3_dimensjoner.svg)
---
class: center
## Produksjonsløype
![Produksjonsløype](../../fagside/assets/img/uke1/pipeline.svg)
---
class: center
## Interoperabilitet
![Produksjonsløype](../../fagside/assets/img/uke1/interoperability.svg)
---
.left-column[
  ## Emnet
]
.right-column[
* Blanding av teori (50%) og praktisk arbeid (50%) med data
* Skripte med python, men kan også gjøres manuelt

]
---
.left-column[
  ## Arbeidskrav
]
.right-column[
- Studentene skal levere to arbeidskrav. Disse består av to praktiske gruppeoppgaver om oppgitte emner.

- Dersom oppgavene ikke blir godkjent, skal studenten få anledning til å levere en ny versjon.

- Arbeidskrav skal være gjennomført innen fastlagt tid, og godkjent av faglærer før studenten kan framstille seg
  til eksamen.
]
---
.left-column[
  ## Arbeidskrav
  ### - En
]
.right-column[
- Beskrivelse av produksjonslinjen din i markdown format
- [Beskrivelse](../../fagside/arbeidskrav/H20-MBIB4140-Arbeidskrav-1.html)
- Kontrol på data
- Vise at du mestrer dataene ved f.eks å ha utført en transformasjon på dataene
- Frist:  __2020-10-09__.

__Merk__: Dere kan bruke [kaggle](https://www.kaggle.com), [any-api](https://any-api.com/), 
    [fellesdatakatalogen](https://informasjonsforvaltning.github.io/felles-datakatalog/) [Oslo kommune](https://github.com/oslokommune/origo-cli)for inspirasjon. Andre kilder kan også brukes.
]
---
.left-column[
  ## Arbeidskrav
  ### - En
  ### - To
]
.right-column[
- [Beskrivelse](../../fagside/arbeidskrav/H20-MBIB4140-Arbeidskrav-2.html)
- Beskriv kvaliteten og mulighetene til å berike dataene
- Frist:  __2020-11-06__.
]
---
## Eksamen
<p>Vurderingsformen er en skriftlig semesteroppgave. Semesteroppgaven kan skrives individuelt - omfang skal da være på
ca. 15 sider (35.000 tegn inkludert mellomrom), eller den kan skrives i gruppe av to til tre studenter. Omfang skal da
    være på ca. 20 sider (46.000 tegn inkludert mellomrom).</p>
<p>Semesteroppgaven skal dekke det meste av emnents innhold i løpet av semesteret.</p>
Det er anbefalt at følgende temaer diskuteres i semesteroppgaven:
- Datasettet / Datasettene / API
- Formater
- Transformasjoner
- Kvalitet / vasking
- Berikelse
- Søk

Merk: At semesteroppgaven handler mer om refleksjon rundt arbeidet som er gjennomgått i løpet av semesteret og skal
    _ikke_ være en teknisk gjennomgang.
---
.left-column[
  ## Sensurveilder
]
.right-column[
* Legges [ut](../../fagside/sensur-veildening-mbib4140.pdf) for kommentar
* Frist 3 uker

]

---
## Strømming
Forelesninger [strømmes](https://connect.uninett.no/mbib4140_h20/) og legges ut i etterkant.
