name: inverse
layout: true
class: center, middle, inverse
---
# MBIB4140
## Metadata og interoperabilitet
### Høst 2019
---
layout: false
# Lærere
&nbsp;
.left-column-fifty[
    ![David Massey](../../assets/img/davm.jpg)<p>davm@oslomet.no</p>
]
.right-column-fifty[
    ![Thomas Sødring](../../assets/img/tsodring.jpg)<p>tsodring@oslomet.no</p>
]
---
class: center, middle
## ... _Vi lever i en datadrevet verden_ ...
---
.left-column[
  ## Data
]
.right-column[
* Data er den nye oljen
* Vi skal foredle denne nye oljen
  * Orientere oss i landskapet og fange data
  * Transformere data
  * Kvalitetsikre data
  * Berike data
* Vi skal jobbe med 'ekte' data
]
---
class: center
## Tre dimensjoner
![Data, Verktøy og prosesser](../../assets/img/uke1/3_dimensjoner.svg)
---
class: center
## Produksjonsløype
![Produksjonsløype](../../assets/img/uke1/pipeline.svg)
---
class: center
## Interoperabilitet
![Produksjonsløype](../../assets/img/uke1/interoperability.svg)
---
.left-column[
  ## Emnet
]
.right-column[
* Blanding av teori (50%) og praktisk arbeid (50%) med data
* Skripte med python, men kan også gjøres manuelt

]
---
.left-column[
  ## Arbeidskrav
]
.right-column[
- Studentene skal levere tre arbeidskrav. Disse består av tre praktiske gruppeoppgaver om oppgitte emner.

- Dersom oppgavene ikke blir godkjent, skal studenten få anledning til å levere en ny versjon.

- Arbeidskrav skal være gjennomført innen fastlagt tid, og godkjent av faglærer før studenten kan framstille seg
  til eksamen.
]
---
.left-column[
  ## Arbeidskrav
  ### - En
]
.right-column[
- Overordnet beskrivelse av casen din
- Beskriv datasettene / APIen du bruker
- Frist:  __15.10.2019__.

__Merk__: Dere kan bruke [kaggle](https://www.kaggle.com), [any-api](https://any-api.com/), eller
    [fellesdatakatalogen](https://fellesdatakatalog.brreg.no/) for inspirasjon. Andre kilder kan også brukes.
]
---
.left-column[
  ## Arbeidskrav
  ### - En
  ### - To
]
.right-column[
- Beskrivelse av casen din i markdown format
- Kontrol på data
- Vise at du mestrer dataene ved f.eks å ha utført en transformasjon på dataene
- Frist:  __02.11.2019__.
]
---
.left-column[
  ## Arbeidskrav
  ### - En
  ### - To
  ### - Tre
]
.right-column[
- Beskriv kvaliteten og mulighetene til å berike dataene
- Beskriv muligheten til å søke i dataene
- Frist:  __01.12.2019__.
]
---
## Eksamen
<p>Vurderingsformen er en skriftlig semesteroppgave. Semesteroppgaven kan skrives individuelt - omfang skal da være på
ca. 15 sider (35.000 tegn inkludert mellomrom), eller den kan skrives i gruppe av to til tre studenter. Omfang skal da
    være på ca. 20 sider (46.000 tegn inkludert mellomrom).</p>
<p>Semesteroppgaven skal dekke det meste av emnents innhold i løpet av semesteret.</p>
Det er anbefalt at følgende temaer diskuteres i semesteroppgaven:
- Datasettet / Datasettene / API
- Formater
- Transformasjoner
- Kvalitet / vasking
- Berikelse
- Søk

Merk: At semesteroppgaven handler mer om refleksjon rundt arbeidet som er gjennomgått i løpet av semesteret og skal
    _ikke_ være en teknisk gjennomgang.

---
## Strømming
Forelesninger [strømmes](https://connect.uninett.no/mbib4140_h19/) og legges ut i etterkant.
