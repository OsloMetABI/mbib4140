<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	version="1.0">
  <xsl:template match="/">
      <xsl:for-each select="articles/article">
TY  - JOUR    
TI  - <xsl:value-of select="title"/>
        <xsl:for-each select="authors/author">
AU  - <xsl:value-of select="."/>
      </xsl:for-each>
JF  - <xsl:value-of select="journal/title"/>
VL  - <xsl:value-of select="journal/volume"/>
IS  - <xsl:value-of select="journal/issue"/>
DO  - <xsl:value-of select="doi"/>
UR  - <xsl:value-of select="url"/>
PY  - <xsl:value-of select="journal/year"/>
SP  - <xsl:value-of select="journal/fromPage"/>
EP  - <xsl:value-of select="journal/toPage"/>
LA  - <xsl:value-of select="language"/>
ER  - 
      </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>