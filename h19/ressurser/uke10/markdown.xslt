<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="text"/>
  <xsl:template match="/">
|article title|authors|journal|volume|issue|doi|url|year|from page|to page|language|
|---|---|---|---|---|---|---|---|---|---|---|
    <xsl:for-each select="articles/article">
    <xsl:value-of select="title"/>|<xsl:for-each select="authors/author"><xsl:value-of select="."/><xsl:text> </xsl:text></xsl:for-each>|<xsl:value-of select="journal/title"/>|<xsl:value-of select="journal/volume"/>|<xsl:value-of select="journal/issue"/>|<xsl:value-of select="doi"/>|<xsl:value-of select="url"/>|<xsl:value-of select="journal/year"/>|<xsl:value-of select="journal/fromPage"/>| <xsl:value-of select="journal/toPage"/>|<xsl:value-of select="language"/>|
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
