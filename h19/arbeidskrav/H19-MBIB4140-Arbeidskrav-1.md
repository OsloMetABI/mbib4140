## MBIB4140-H19 Arbeidskrav 1
Arbeidskravet er et refleksjonsnotat på ca. 1 til 1 1/2 sider som dokumenterer hvilken data du ønsker å jobbe med i løpet av semesteret. Kilden til dataene skal også beskrives.

Dere kan selv velge hvilken kilder / APIer dere vil bruke, men følgende kan være til inspirasjon:

 * [kaggle](https://www.kaggle.com)
 * [any-api](https://any-api.com/)
 * [fellesdatakatalogen](https://fellesdatakatalog.brreg.no/) 

Frist: 15.10.2019.

Arbeidskravet lastes opp i Canvas "Arbeidskrav 1"