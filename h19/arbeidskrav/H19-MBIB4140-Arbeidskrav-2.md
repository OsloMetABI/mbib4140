## MBIB4140-H20 Arbeidskrav 2
Arbeidskravet er et refleksjonsnotat på ca. 1 til 1 1/2 sider som dokumenterer produksjonslinjen du jobber med.  Du skal redegjøre for hvordan du opplever kvaliteten på datanene og hvordan dataene kan berikes. Arbeidskravet leveres om en markdown fil (.md) 

Frist: 2020-11-06.

Arbeidskravet sendes på epost til Thomas (tsoding (a) oslomet no). Merk eposten med "MBIB4140-H20 Arbeidskrav 2"
