## MBIB4140-H19 Arbeidskrav 3
Arbeidskravet er et refleksjonsnotat på ca. 2 til 3 sider som dokumenterer arbeidet på produksjonslinjen din.

Arbeidskravet leveres om en markdown fil (.md) 

Frist: 2019-12-01.

Arbeidskravet lastes opp i Canvas "Arbeidskrav 3"